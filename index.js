

//GET METHOD

//console.log(fetch(`https://jsonplaceholder.typicode.com/posts`))
//fetch(`https://jsonplaceholder.typicode.com/posts`)
/*,
method: "POST",
headers: {

},
body: JSON.stringify({
    email: document.getElementById('email').value,
    password: document.getElementById('password').value
})*/
//.then( data => console.log(data));
    //name anything for variable. representes response object
// .then( data => data.json() )
// .then( data => {
//     //console.log(data)

//     data.forEach(element =>{
//         console.log(element)
//     })
// } )

//GET METHOD for a specific document

//id in end of link
/*
fetch(`https://jsonplaceholder.typicode.com/posts/55`)
.then(response => response.json())
.then(response => {
    console.log(response)
})
*/

//POST METHOD
/*
fetch(`https://jsonplaceholder.typicode.com/posts`, {
    method: "POST",
    headers: {
        "Content-type": "application/json"
    },
    body: JSON.stringify({
        title: "New Post",
        body: "Hello World",
        userid: 1
    })
})
.then(response => response.json())
.then( response => {
    console.log(response)
})
*/

//PUT METHOD to update a specific document
/*
fetch(`https://jsonplaceholder.typicode.com/posts/1`, {
    method: "PUT",
    headers: {
        "Content-type": "application/json"
    },
    body: JSON.stringify({
        title: "Updated Post",
        body: "Hello updated post",
        userid: 1
    })
})
.then(res => res.json())
.then(res => {
    console.log(res)
})
*/

// PATCH METHOD
    //replaces particular field only
    //applies update to a particular
/*
fetch(`https://jsonplaceholder.typicode.com/posts/1`, {
    method: "PATCH",
    headers: {
        "Content-type": "application/json"
    },
    body: JSON.stringify({
        title: "Corrected post thru patch method"
    })
})
.then(data => data.json())
.then(data => {
    console.log(data)
})
*/

//DELETE METHOD deletes a particular document
/*
fetch(`https://jsonplaceholder.typicode.com/posts/1`, {
    method: "DELETE"
})
*/

//FILTERING METHOD
    //filteringdata by sending a query via the URL
    //single query: endpoint?parameter=value
    //multiple query: endpoint?parameterA=valueA&parameterB=valueB
    //?-sets boundary between endpoint and query
    //&-include other parameter
fetch(`https://jsonplaceholder.typicode.com/posts?userId=1&id=1`)
.then(data => data.json())
.then(data => {
    console.log(data)
})