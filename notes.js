//Anatomy of a client request

// let request = {
//     "url":"/addCourse",
//     "method": "POST",
//     "headers": {"Content-type", "application/json"};
//     "bdoy": {
//         "name": "Node JS and API and REST"
//     }
// }

// console.log(`Hello`);

/*
FETCH METHOD
        - fetch API allows you to asynchronously request for a resource (data)
        - java script method to send request from server
        - done in client side

    syntax: 
        fetch("URL",{options})
            //if get, no need to write options

    then( callback fn) method
        -handles promise object (response?)
        -promise can be either a resolve or reject
*/

//example of fetch and promise
fetch("http://localhost:3000", {
    method: "POST",
    headers: {
        "Content-Type": "application/json"},
    body: JSON.stringify({
        //property : value
    })
}).then(response => response.json()) //parse the data from string to JS data type
.then(response => console.log(response));

//get method